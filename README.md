# Projet MIAGE Réseau
## POIROT Cloé - KELBERT Paul - LECOMPTE Yvan

### Téléchargement
--------------------------
Pour initialiser le projet, il suffit de suivre ces commandes ci-dessous :
```bash
git clone https://gitlab.com/m9215/reseau/stomp.git
cd stomp
npm install
```

### Lancement du serveur
-------------------------
Une seule commande est nécessaire :  `node .\src\server.js`

### Utilisation du client
-------------------------
Si jamais le client de test ne fonctionne pas avec le serveur, merci d'utiliser le client fourni dans ./client/index.html

Il suffit juste d'ouvrir le fichier dans le navigateur !


### Librairies :
------------------------
Dans le fichier `package.json`
```json
{
  "name": "projet-stomp",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.18.1",
    "ws": "^8.6.0"
  },
  "devDependencies": {
    "@types/express": "^4.17.13",
    "@types/ws": "^8.5.3",
    "typescript": "^4.6.4"
  }
}
```
