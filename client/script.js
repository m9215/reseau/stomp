const host = `ws://localhost:8999/`
const ws = new WebSocket(host)

const connect = () => {
  const username = document.getElementById('username').value
  if (!username) {
    alert("Merci d'entrer un nom d'utilisateur")
    return
  }

  ws.send(`CONNECT
  accept-version:1.0
  host:${host}
  login:${username}
  ^@`)
}

const subscribe = () => {
  const topic = document.getElementById('topic').value.toUpperCase()
  if (!topic) {
    alert("Merci d'entrer un topic")
    return
  }

  if (document.getElementById(`topic-${topic}`)) {
    alert("Vous êtes déjà abonné à ce topic")
    return
  }

  ws.send(`SUBSCRIBE
  id:${Date.now()}
  destination:/${topic}
  
  ^@`)
}

const unsubscribe = (id) => {
  ws.send(`UNSUBSCRIBE
  id:${id}
  
  ^@`)
}

const sendMessage = (id) => {
  const message = document.getElementById(`topic-container-new-${id}`).value
  if (!message) {
    alert("Merci d'entrer un message")
    return
  }
  ws.send(`SEND
  destination:${id}
  content-type:text/plain
  sender:${document.getElementById('username').value}
  
  ${message}
  ^@`)
}


ws.onmessage = ({ data }) => {
  const lines = data.split('\n')
  if (data.startsWith('CONNECTED')) {
    document.getElementById('not-connected').style.display = 'none'
    document.getElementById('connected').style.display = 'flex'
    return;
  }
  if (data.startsWith('RECEIPT')) {
    const type = getValueAt(lines, 1)
    const topic = document.getElementById('topic').value.toUpperCase()
    if (type === 'SUBSCRIBE') {
      document.getElementById('topic-list-ul').innerHTML += `<li id="topic-${getValueAt(lines, 2)}"><strong title="Se désabonner" onclick="unsubscribe(${getValueAt(lines, 2)})"> X </strong> -> ${topic}</li>`
      document.getElementById('followed-topics').innerHTML += `
        <div class="topic-container" id="topic-container-${getValueAt(lines, 2)}">
          <h2>${topic}</h2>
          <div style="display: flex; flex-direction: column; max-width: 50%;">
            <div style="margin-bottom:2%; min-height: 500px; border: solid 1px; overflow: scroll; padding:2%" class="topic-container-content" id="topic-container-content-${getValueAt(lines, 2)}" readonly></div>
            <div>
              <textarea cols=50 id="topic-container-new-${getValueAt(lines, 2)}"></textarea>
              <button onclick="sendMessage(${getValueAt(lines, 2)})">Envoyer</button>
            </div>
          </div>
        </div>
      `
      document.getElementById('topic').value = ""
      return
    }
    if (type === 'UNSUBSCRIBE') {
      document.getElementById(`topic-${getValueAt(lines, 2)}`).remove()
      document.getElementById(`topic-container-${getValueAt(lines, 2)}`).remove()
      return
    }
  }
  if (data.startsWith('MESSAGE')) {
    let content =lines[6]
    for (let i = 7; i < lines.length-1; i++) {
      content += `${lines[i]}<br>`
    }
    const newMessage = `
     <strong>${getValueAt(lines, 4)}</strong> >>> ${content} <br>
    `
    document.getElementById(`topic-container-content-${getValueAt(lines, 2)}`).innerHTML += newMessage
  }
}



const getValueAt = (data, id) => {
  return data[id].split(':')[1].replace("\r", "")
}