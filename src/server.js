"use strict";
exports.__esModule = true;
var express = require("express");
var http = require("http");
var WebSocket = require("ws");
var app = express();
//initialize a simple http server
var server = http.createServer(app);
//initialize the WebSocket server instance
var wss = new WebSocket.Server({ server: server });
var userSubscribeList = [];
var userTopicList = [];
wss.on('connection', function (ws) {
    //connection is up, let's add a simple simple event
    ws.on('message', function (message) {
        var receive = message.toString();
        var data = receive.split('\n');
        // if message start with CONNECT
        if (receive.startsWith('CONNECT')) {
            console.log("".concat(getValueAt(data, 3), " CONNECTED"));
            ws.send("CONNECTED\n          version:1.1\n          \n          ^@");
            return true;
        }
        // if message start with SUBSCRIBE
        if (receive.startsWith('SUBSCRIBE')) {
            var id = getValueAt(data, 1);
            userSubscribeList.push({ id: id, ws: ws });
            userTopicList.push({ id: id, topic: getValueAt(data, 2) });
            console.log("A user subscribe to ".concat(getValueAt(data, 2)));
            ws.send("RECEIPT\n          receipt-id:SUBSCRIBE\n          message-id:".concat(id, "\n          \n          ^@"));
            return;
        }
        // if message start with UNSUBSCRIBE
        if (receive.startsWith('UNSUBSCRIBE')) {
            var id_1 = getValueAt(data, 1);
            var index = userSubscribeList.findIndex(function (item) { return item.id === id_1; });
            userSubscribeList.splice(index, 1);
            var indexTopic = userTopicList.findIndex(function (item) { return item.id === id_1; });
            userTopicList.splice(indexTopic, 1);
            console.log("A user unsubscribe to a topic");
            ws.send("RECEIPT\n          receipt-id:UNSUBSCRIBE\n          message-id:".concat(id_1, "\n          \n          ^@"));
            return;
        }
        // if message start with PUBLISH
        if (receive.startsWith('SEND')) {
            var id_2 = getValueAt(data, 1);
            var sender_1 = getValueAt(data, 3);
            var message_1 = '';
            for (var i = 5; i < data.length - 1; i++) {
                message_1 += data[i] + '\n';
            }
            var topic_1 = userTopicList.find(function (item) { return item.id === id_2; });
            var allIds_1 = [];
            userTopicList.filter(function (item) { return item.topic === topic_1.topic; }).forEach(function (item) {
                allIds_1.push({ id: item.id });
            });
            userSubscribeList.filter(function (item) { return allIds_1.find(function (item2) { return item2.id === item.id; }); }).forEach(function (item) {
                item.ws.send("MESSAGE\n            subscription:0\n            message-id:".concat(item.id, "\n            destination:").concat(id_2, "\n            sender:").concat(sender_1, "\n            content-type:text/plain\n            \n            ").concat(message_1, "\n            ^@"));
            });
        }
    });
});
wss.on('error', function (error) {
    console.error(error);
});
//start our server
server.listen(process.env.PORT || 8999, function () {
    console.log("Server started ! :)");
});
var getValueAt = function (data, id) {
    return data[id].split(':')[1].replace("\r", "");
};
