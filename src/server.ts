import * as express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';

const app = express();
//initialize a simple http server
const server = http.createServer(app);
//initialize the WebSocket server instance
const wss = new WebSocket.Server({ server });

const userSubscribeList: any = [];
const userTopicList: any = [];


wss.on('connection', (ws: WebSocket) => {
    //connection is up, let's add a simple simple event
    ws.on('message', (message: string) => {
        const receive = message.toString()
        const data = receive.split('\n');
        // if message start with CONNECT
        if (receive.startsWith('CONNECT')) {
          console.log(`${getValueAt(data,3)} CONNECTED`);
          ws.send(`CONNECTED
          version:1.1
          
          ^@`);
          return true;
        }
        // if message start with SUBSCRIBE
        if (receive.startsWith('SUBSCRIBE')) {
          const id = getValueAt(data,1);
          userSubscribeList.push({id, ws});
          userTopicList.push({id, topic: getValueAt(data,2)});

          console.log(`A user subscribe to ${getValueAt(data,2)}`);
          ws.send(`RECEIPT
          receipt-id:SUBSCRIBE
          message-id:${id}
          
          ^@`)
          return;
        }
        // if message start with UNSUBSCRIBE
        if (receive.startsWith('UNSUBSCRIBE')) {
          
          const id = getValueAt(data,1);
          const index = userSubscribeList.findIndex((item: { id: string; }) => item.id === id);
          userSubscribeList.splice(index, 1);
          const indexTopic = userTopicList.findIndex((item: { id: string; }) => item.id === id);
          userTopicList.splice(indexTopic, 1);

          console.log(`A user unsubscribe to a topic`);
          ws.send(`RECEIPT
          receipt-id:UNSUBSCRIBE
          message-id:${id}
          
          ^@`)
          return;
        }
        // if message start with PUBLISH
        if (receive.startsWith('SEND')) {
          const id = getValueAt(data,1);
          const sender = getValueAt(data,3);
          let message = '';
          for (let i = 5; i < data.length-1; i++) {
            message += data[i] + '\n';
          }
          const topic = userTopicList.find((item: { id: string; topic: string; }) => item.id === id);
          let allIds: { id: string; }[] = [];

          userTopicList.filter((item: { id: string; topic: string; }) => item.topic === topic.topic).forEach((item: { id: string; topic: string; }) => {
            allIds.push({id: item.id});
          });

          userSubscribeList.filter((item: { id: string; }) => allIds.find((item2: { id: string; }) => item2.id === item.id)).forEach((item: { id: string; ws: WebSocket}) => {
            item.ws.send(`MESSAGE
            subscription:0
            message-id:${item.id}
            destination:${id}
            sender:${sender}
            content-type:text/plain
            
            ${message}
            ^@`)
          })
        }
    });
});

wss.on('error', (error: Error) => {
    console.error(error);
});

//start our server
server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started ! :)`);
});

const getValueAt = (data: any ,id: number) => {
  return data[id].split(':')[1].replace("\r", "")
}
